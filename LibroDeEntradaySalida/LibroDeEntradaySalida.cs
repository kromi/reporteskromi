﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibroDeEntradaySalida
{
    public partial class LibroDeEntradaySalida : Form
    {
        public LibroDeEntradaySalida()
        {
            InitializeComponent();
        }

        private void LibroDeEntradaySalida_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
